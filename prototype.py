import asyncio
import discord
import os
import logging

CONFIG_DEFAULT_GUILD=0x0
CONFIG_DEFAULT_REPORT=0x0

TGUILD = int(CONFIG_DEFAULT_GUILD if not "BOT_RECORD_GUILD" in os.environ else os.environ["BOT_RECORD_GUILD"])
REPORT = int(CONFIG_DEFAULT_REPORT if not "BOT_REPORT_CHANN" in os.environ else os.environ["BOT_REPORT_CHANN"])
TOKEN  = None if not "BOT_TOKEN" in os.environ else os.environ["BOT_TOKEN"]

if TOKEN is None:
    with open(".token", "r", encoding="utf-8") as t:
        TOKEN = t.read().strip()

INVITES = {}
logging.basicConfig(level=logging.INFO)
LOG = logging.getLogger(name="local")
LOG.setLevel(logging.DEBUG)

LOG.debug("GUILD: {0}".format(TGUILD))
LOG.debug("REPRT: {0}".format(REPORT))

if REPORT == 0 or TGUILD == 0:
    LOG.error("Sanity check failed: REPORT OR TGUILD")
    exit(1)

client = discord.Client()


@client.event
async def on_ready():
    svr = client.get_guild(TGUILD)
    if svr is None:
        LOG.error("<< ERROR >>: Server [" + TGUILD + "] was not found !")
        exit(1)

    LOG.debug("Querying server")

    try:
        invites = await svr.invites()
    except discord.Forbidden:
        LOG.error("<< ERROR >>: Missing permissions to gather invites !")
        exit(1)

    LOG.debug("--- << INVITES >>")

    for inv in invites:
        LOG.debug("{0}: [{1}/{2}] ? {3}".format(inv.code, inv.uses,
                                                inv.max_uses if inv.max_uses > 0 else "INFINITE", inv.inviter.name))
        INVITES[inv.code] = inv


@client.event
async def on_member_join(member):
    if member.guild.id != TGUILD:
        return

    invites = await member.guild.invites()

    for inv in invites:
        if not inv.code in INVITES:
            INVITES[inv.code] = inv

            if inv.uses > 0:
                await push_event(inv, member)
        else:
            if inv.uses > INVITES[inv.code].uses:
                await push_event(inv, member)


async def push_event(invite, member):
    LOG.debug("Recieved InvJoin")
    LOG.debug("--- INVITE")
    LOG.debug("Code:       {0}".format(invite.code))
    LOG.debug("Revoked:    {0}".format(invite.revoked))
    LOG.debug("Created_at: {0}".format(invite.created_at))
    LOG.debug("Uses:       {0}".format(invite.uses))
    LOG.debug("MaxUses:    {0}".format(
        invite.max_uses if invite.max_uses > 0 else "INFINITE"))
    LOG.debug("Channel:    {0}".format(invite.channel))
    LOG.debug("ID:         {0}".format(invite.id))
    LOG.debug("Inviter:    {0}".format(invite.inviter.name))
    LOG.debug("--- USER")
    LOG.debug("Name:       {n}#{d:04}".format(n=member.name, d=int(member.discriminator)))
    LOG.debug("Nick:       {0}".format(member.nick))
    LOG.debug("ID:         {0}".format(member.id))
    LOG.debug("Bot:        {0}".format(member.bot))

    report = client.get_channel(REPORT)
    if report is None:
        print("Report channel does not exist")
        return

    yn = lambda v: "Yes" if v else "No"
    rr = "**INVITE USED**\n\
        ```\n\
        [{code}]: {uses}/{muses} -> {channel} [Active {active}]\n\
        |-\\ Inviter\n\
        | | Name:  {iname}\n\
        | | Nick:  {inick}\n\
        | | ID:    {iid}\n\
        | | Bot:   {ibot}\n\
        | Created: {created}\n\
        | ID:      {id}\n\
        [Invited: {nname}#{ndiscrim} ({nid}{nbot})]\n\
        ```".format(
            code = invite.code,
            uses = invite.uses,
            muses = invite.max_uses,
            channel = invite.channel.mention,
            active = yn(invite.revoked),
            iname = invite.inviter.name,
            inick = invite.inviter.nick.replace("`", ""),
            iid = invite.inviter.id,
            ibot = yn(invite.inviter.bot),
            created = invite.created_at,
            id = invite.id,
            nname = member.name,
            ndiscrim = member.discriminator,
            nid = member.id,
            nbot = " !BOT" if member.bot else ""
        )
    
    await report.send(rr)

client.run(TOKEN, bot=True)
