use parking_lot::Mutex;
use serenity::{
	model::{
		gateway::Ready,
		guild::Member,
		id::{ChannelId, GuildId},
		invite::RichInvite,
	},
	prelude,
	prelude::Context,
};
use std::{collections::HashMap, sync::Arc};

pub struct EventHandler {
	invites: Arc<Mutex<HashMap<String, RichInvite>>>,
	target:  u64,
	channel: u64,
}

impl prelude::EventHandler for EventHandler {
	fn ready(&self, ctx: Context, ready: Ready) {
		// TODO: OnReady
		log::debug!("Recieved READY");
		log::debug!("=> << USER >>");
		log::debug!("=> ID: {}", ready.user.id);
		log::debug!("=> NAME: {}#{}", ready.user.name, ready.user.discriminator);

		let invites = match GuildId(self.target).invites(&ctx.http) {
			Ok(ilist) => ilist,
			Err(e) => {
				log::warn!("failed to retrieve invites");
				log::warn!("{:?}", e);
				return;
			},
		};

		let mut lc = self.invites.lock();
		for invite in invites {
			let max_uses = if invite.max_uses == 0 {
				"??".to_owned()
			} else {
				format!("{:02}", invite.max_uses)
			};

			log::debug!(
				"{code} [{u:02}/{m}] => {i}#{d} ({id}) ",
				code = invite.code,
				u = invite.uses,
				m = max_uses,
				i = invite.inviter.name,
				d = invite.inviter.discriminator,
				id = invite.inviter.id
			);

			let _ = lc.insert(invite.code.clone(), invite);
		}
	}

	fn guild_member_addition(&self, ctx: Context, gid: GuildId, mem: Member) {
		if gid.0 != self.target {
			return;
		}

		log::debug!("Member joined. processing invites...");
		let invites = match gid.invites(&ctx.http) {
			Ok(invlist) => invlist,
			Err(e) => {
				log::warn!("failed to retrieve invites");
				log::warn!("{:?}", e);
				return;
			},
		};

		let mut lc = self.invites.lock();
		for invite in invites {
			if let Some(stored) = lc.get(&invite.code) {
				log::debug!(
					"Existing invite found [{}] {} -> {}",
					invite.code,
					stored.uses,
					invite.uses
				);
				if invite.uses > stored.uses {
					self.report_invite(ctx, &invite, &mem);
					lc.insert(invite.code.clone(), invite);
					break;
				}
			} else {
				log::debug!("New invite: [{}] -> {}", invite.code, invite.uses);
				if invite.uses == 1 {
					self.report_invite(ctx, &invite, &mem);
				} else {
					log::warn!("we seem to have gotten a invite faster than should be possible");
					log::debug!("{:?}", invite);
				}
				lc.insert(invite.code.clone(), invite);
				break;
			}
		}
	}
}

impl EventHandler {
	pub fn new(target: u64, channel: u64) -> Self {
		EventHandler {
			invites: Arc::new(Mutex::new(HashMap::default())),
			target,
			channel,
		}
	}

	fn report_invite(&self, ctx: Context, invite: &RichInvite, member: &Member) {
		let max_uses = if invite.max_uses == 0 {
			"??".to_owned()
		} else {
			format!("{:02}", invite.max_uses)
		};

		let formatted = {
			let inv_user = member.user.read();

			let channel = invite.channel.name.replace("`", "").replace("@", "(at)");
			let inviter = invite.inviter.name.replace("`", "").replace("@", "(at)");
			let invited = inv_user.name.replace("`", "").replace("@", "(at)");

			format!(
				"```\n[{code}]: {uses}/{max_uses} for #{channel} -> created: \
				 {invite_created}\nInviter: {inviter_name}#{inviter_discrim} \
				 ({inviter_id}{inviter_bot} -> created: {inviter_created})\nInvited: \
				 {invited_name}#{invited_discrim} ({invited_id}{invited_bot} -> created: \
				 {invited_created})\n```",
				code = invite.code,
				uses = invite.uses,
				max_uses = max_uses,
				channel = channel,
				invite_created = invite.created_at,
				// Inviter
				inviter_name = inviter,
				inviter_discrim = invite.inviter.discriminator,
				inviter_id = invite.inviter.id.0,
				inviter_bot = if invite.inviter.bot { " !BOT" } else { "" },
				inviter_created = invite.inviter.created_at(),
				// Invited
				invited_name = invited,
				invited_discrim = inv_user.discriminator,
				invited_id = inv_user.id.0,
				invited_bot = if inv_user.bot { " !BOT" } else { "" },
				invited_created = inv_user.created_at()
			)
		};

		log::debug!("posting event");
		if let Err(why) = ChannelId(self.channel).say(&ctx.http, &formatted) {
			log::warn!("failed to post update!");
			log::warn!("error: {:?}", why);
			log::warn!("message text:\n{}", formatted);
		}
	}
}