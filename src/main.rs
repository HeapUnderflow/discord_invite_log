mod handler;

use serenity::prelude::*;

macro_rules! maybe {
	($e:expr, $m:expr) => {{
		match $e {
			Some(v) => v,
			None => {
				log::error!("error: {}", $m);
				return Err(1);
			},
			}
		}};
}

fn main() {
	match realmain() {
		Ok(_) => (),
		Err(v) => ::std::process::exit(v),
	}
}

fn realmain() -> Result<(), i32> {
	if let Err(why) = kankyo::init() {
		use std::io::ErrorKind;
		match why.kind() {
			ErrorKind::NotFound => (),
			_ => {
				eprintln!("Error while loading .env file");
				eprintln!("\n{:#?}", why);
				return Err(1);
			},
		}
	}

	env_logger::init();

	let token = maybe!(kankyo::key("TOKEN"), "missing token");
	let target: u64 = maybe!(
		maybe!(kankyo::key("TARGET_GUILD"), "missing target guild")
			.parse()
			.ok(),
		"failed to parse guild"
	);

	let report: u64 = maybe!(
		maybe!(kankyo::key("REPORT_CHANNEL"), "Missing target channel")
			.parse()
			.ok(),
		"failed to parse channel"
	);

	let mut client = match Client::new(&token, handler::EventHandler::new(target, report)) {
		Ok(cl) => cl,
		Err(why) => {
			log::error!("failed to create the client");
			log::error!("why: {:?}", why);
			return Err(1);
		},
	};

	if let Err(e) = client.start() {
		log::error!("client terminated: {:?}", e);
	}

	Ok(())
}
